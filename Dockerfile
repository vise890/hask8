# vi: set ft=Dockerfile :
FROM haskell:8
MAINTAINER Martino Visintin

RUN apt-get update && \
    apt-get dist-upgrade --yes && \
    apt-get install --yes make xz-utils && \
    apt-get purge --auto-remove --yes && \
    stack setup
