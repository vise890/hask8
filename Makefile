all: docker_build docker_push

docker_build:
	docker build --tag hask8:latest .

docker_push:
	docker push hask8:latest

stack_dump_deps:
	@echo "--> this is meant to be run in a stack pj"
	stack list-dependencies | cut -d' ' -f1 | xargs
